# README #

This is a collection of AI-RELATED  modules.

Eclipse is the preferred IDE, or any other plain text editor. This is to ensure easy integrability and avoid tracking unecessary files.

### Contribution guidelines ###

All contributors should ensure proper modularity is put into practice. Already written code and given project structure is provided to aid this purpose. Proper documentation is also highly advised.