package ai;

import java.util.List;

/**
 * contains helper methods to carry out basic
 * functions needed by the ai module
 */
public class Utilities {
	
	/**
	 * helper method to display graph as adjacency matrix
	 */
	public static void printGraph(Graph graph){
		List<Node> nodes = graph.getNodes();
		int nodeSize = nodes.size();
		System.out.println();
		
		System.out.print(padRight("  ", 5));	
		for (int x = 0; x < nodeSize; x++){
			System.out.print(padRight(nodes.get(x).toString(), 5));
		}
		System.out.println();
		System.out.println("----------------------------");
		for (int i = 0; i < nodeSize; i++){
			System.out.print(nodes.get(i));
			System.out.print(padRight("|", 5));				
			for (int j = 0; j < nodeSize; j++){
				System.out.print(padRight(graph.getAdjacencyMatrix().get(i).get(j).toString(), 5));				
			}	
			System.out.println();
		}
	}
	
	/**
	 * helper method to print a list of nodes
	 * @param list of nodes to be printed
	 */
	public static void printNodeList(List<Node> list) {
		
		System.out.println();
		for (Object object : list) {
			System.out.print(object + "\t");
		}
	}
	
	/**
	 * pad a string at the end 
	 * @param s - the string to be padded
	 * @param n - the number of spaces occupied
	 * @return padded string
	 */
	private static String padRight(String s, int n) {
	     return String.format("%1$-" + n + "s", s);  
	}
	
	public static Graph buildGraph1(){
		Graph graph = new Graph();
		graph.addEdge(new Node("A"), new Node("B"));
		graph.addEdge(new Node("A"), new Node("C"));
		graph.addEdge(new Node("B"), new Node("D"));
		graph.addEdge(new Node("B"), new Node("E"));
		graph.addEdge(new Node("C"), new Node("F"));
		graph.addEdge(new Node("D"), new Node("G"));
		graph.addEdge(new Node("E"), new Node("H"));
		graph.addEdge(new Node("F"), new Node("H"));
		graph.addEdge(new Node("G"), new Node("I"));
		graph.addEdge(new Node("H"), new Node("J"));
		graph.addEdge(new Node("H"), new Node("K"));
		graph.addEdge(new Node("I"), new Node("L"));
		graph.addEdge(new Node("J"), new Node("L"));
		graph.addEdge(new Node("K"), new Node("M"));
		graph.addEdge(new Node("L"), new Node("N"));
		graph.addEdge(new Node("M"), new Node("N"));
		return graph;
	}
	
	public static Graph buildGraph2(){
		Graph g = new Graph(true);
		Node A = new Node("A");
		Node B = new Node("B", A);
		Node C = new Node("C", A);
		Node D = new Node("D", B);
		Node E = new Node("E", B);
		Node F = new Node("F", C);
		Node G = new Node("G", C);
		g.addEdge(A, B);
		g.addEdge(A, C);
		g.addEdge(B, D);
		g.addEdge(B, E);
		g.addEdge(C, F);
		g.addEdge(C, G);
		return g;
	}
}
