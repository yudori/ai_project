package ai;

import java.util.List;
import java.util.Scanner;

import javax.swing.SwingUtilities;

import ai.Search.UninformedSearch.BFS;
import ai.UI.GraphVisualizer;

public class Main {

	public static void main(String[] args) {

		Graph g = Graph.generateRandomGraph();
		Utilities.printGraph(g);
		SwingUtilities.invokeLater(new Runnable() {
			 
	        @Override
	        public void run() {
	            new GraphVisualizer().setVisible(true);
	        }
	    });
	}
}
