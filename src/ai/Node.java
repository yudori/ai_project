package ai;

import java.awt.Color;
import java.awt.Point;

/**
 * This class is an ADT representing a node structure 
 * @author ai_team group 3
 *
 */
public class Node {
	
	private static final int DEFAULT_POINT_X = 0;
	private static final int DEFAULT_POINT_Y = 0;	
	private static final int DEFAULT_SIZE = 30;
	public static final Color DEFAULT_COLOR = Color.BLUE;

	private String tag;
	private Point point;
	private Color color;
	private int size;
	private  int depth;
    private  int Cost;
    private  Node Parent;
	
	public Node(String tag){
		this(tag, null);
		
	}
	 public Node(String tag, Node Parent)
     {
         this(tag, Parent, 0);     
     }
	 public Node(String tag, Node Parent, int Cost)
     {
       this.Parent = Parent;
       this.Cost = Cost;
       if (Parent == null)
          this.depth = 0; 
       else
          this.depth = Parent.depth + 1;  
        setTag(tag);
        setSize(DEFAULT_SIZE);
		setColor(DEFAULT_COLOR);
		setPosition(new Point(DEFAULT_POINT_X, DEFAULT_POINT_Y));
     }
	
	public void setTag(String tag){
		this.tag = tag;
	}
	
	public String getTag(){
		return this.tag;
	}
	
	public void setPosition(Point point){
		this.point = point;
	}
	
	public Point getPosition(){
		return this.point;
	}
	public int getDepth(){
		return this.depth;
	}
	public int getCost(){
		return this.Cost;
	}
	public void setColor(Color color){
		this.color = color;
	}
	
	public Color getColor(){
		return this.color;
	}
	
	public void setSize(int size){
		this.size = size;
	}
	
	public int getSize(){
		return this.size;
	}
	
	public Point getMidPoint(){
		int x = point.x + (int)(size / 2);
		int y = point.y + (int)(size / 2);
		return new Point(x, y);
	}
	
	@Override
	public boolean equals(Object object){
		
		boolean isEqual = false;		
		if (object != null && object instanceof Node){
			isEqual = (this.tag.equalsIgnoreCase(((Node) object).getTag()));
		}
		return isEqual;
	}
	
	@Override
	public String toString(){
		return tag;
	}
	
}
