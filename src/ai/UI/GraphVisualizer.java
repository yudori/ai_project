package ai.UI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;

import ai.Graph;
import ai.Node;
import ai.Utilities;
import ai.Search.Search;
import ai.Search.UninformedSearch.BFS;
import ai.Search.UninformedSearch.BIDI;
import ai.Search.UninformedSearch.DFS;
import ai.Search.UninformedSearch.DLS;
import ai.Search.UninformedSearch.IDS;
import ai.Search.UninformedSearch.UCS;

public class GraphVisualizer extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public static final String TYPE_DFS = "Depth-first Search";
	public static final String TYPE_DLS = "Depth-limited Search";
	public static final String TYPE_IDS = "Iterative Deepening Search";
	public static final String TYPE_BFS = "Breadth-first Search";
	public static final String TYPE_BIDI = "Bidirectional Search";
	public static final String TYPE_UCS = "Uniform Cost Search";
	
	
	GraphCanvas canvas;
	JTextField txtStartNode, txtGoalNode, txtDepth;
	List<String> searchMethods;
	ButtonGroup grpSearch;

	JButton btnVisualize;
	String currentSelected;
	
	public GraphVisualizer(){
		this.setPreferredSize(new Dimension(960, 720));
		//this.setResizable(false);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.canvas = new GraphCanvas(Graph.generateRandomGraph());
		
		JPanel rightPanel = new JPanel(new GridLayout(20,1));
		JLabel label1 = new JLabel("Start Node");
		JLabel label2 = new JLabel("Goal Node");
		JLabel label3 = new JLabel("Depth");
		txtStartNode = new JTextField();
		txtGoalNode = new JTextField();
		txtDepth = new JTextField();
		btnVisualize = new JButton("Visualize");
		btnVisualize.addActionListener(this);
		
		
		searchMethods = new ArrayList<>();
		searchMethods.add(TYPE_DFS);
		searchMethods.add(TYPE_DLS);
		searchMethods.add(TYPE_IDS);
		searchMethods.add(TYPE_BFS);
		searchMethods.add(TYPE_BIDI);
		searchMethods.add(TYPE_UCS);
		
		grpSearch = new ButtonGroup();
		for (String s : searchMethods){
			JRadioButton btn = new JRadioButton(s);
			btn.addItemListener(new ItemListener() {
				 
			    @Override
			    public void itemStateChanged(ItemEvent event) {
			        int state = event.getStateChange();
			        if (state == ItemEvent.SELECTED) {			 
			            // do something when the button is selected
			        	if (((JRadioButton)event.getSource()).getText().equals(TYPE_DLS) ||
			        			((JRadioButton)event.getSource()).getText().equals(TYPE_IDS)){
			        		
							canvas.setGraph(Utilities.buildGraph2());
							canvas.randomizeNodePositions();
							canvas.repaint();
			        	}
			        		
			        	currentSelected = ((JRadioButton)event.getSource()).getText();
			        } 
			    }
			});
			grpSearch.add(btn);
			rightPanel.add(btn);
		}
		rightPanel.add(label1);		
		rightPanel.add(txtStartNode);
		rightPanel.add(label2);
		rightPanel.add(txtGoalNode);
		rightPanel.add(label3);
		rightPanel.add(txtDepth);
		rightPanel.add(btnVisualize);
		
		
		this.add(rightPanel, BorderLayout.EAST);
		this.add(canvas, BorderLayout.CENTER);
		this.pack();
		canvas.randomizeNodePositions();
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnVisualize){
			
			Search search = null;
			switch (currentSelected){
			case TYPE_DFS:
				search = new DFS(canvas.getGraph(), txtStartNode.getText(), txtGoalNode.getText());
				break;
			case TYPE_DLS:
				search = new DLS(canvas.getGraph(), txtStartNode.getText(), txtGoalNode.getText(), toInt(txtDepth.getText()));
				break;
			case TYPE_IDS:
				search = new IDS(canvas.getGraph(), txtStartNode.getText(), txtGoalNode.getText());
				break;
			case TYPE_BFS:
				search = new BFS(canvas.getGraph(), txtStartNode.getText(), txtGoalNode.getText());
				break;
			case TYPE_BIDI:
				search = new BIDI(canvas.getGraph(), txtStartNode.getText(), txtGoalNode.getText());
				break;
			case TYPE_UCS:
				search = new UCS(canvas.getGraph(), txtStartNode.getText(), txtGoalNode.getText());
				break;
			}
			if (search != null)
				canvas.visualize(search);
		}
	}

	private int toInt(String str){
		int d = 0;
		try{
			d = Integer.parseInt(str);
		}catch (NumberFormatException ex){
			JOptionPane.showMessageDialog(this, "Depth must be a number!");
			return 0;
		}
		return d;
	}
}
