package ai.UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

import ai.Graph;
import ai.Node;
import ai.Search.Search;
import ai.Search.SearchStatus;
import ai.Search.UninformedSearch.BFS;

public class GraphCanvas extends JPanel implements MouseListener, MouseMotionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Graph graph;
	private Node selectedNode;

	public GraphCanvas(Graph graph){
		this.graph = graph;		

		//this.setMinimumSize(new Dimension(640, 640));
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	public Graph getGraph(){
		return this.graph;
	}
	
	public void setGraph(Graph graph){
		this.graph = graph;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		drawEdges(g);
		for (Node node : graph.getNodes()){
			drawNode(node, g);
		}		
	}
	
	public void randomizeNodePositions(){
		List<Node> nodes = graph.getNodes();
		int xLimit = this.getSize().width;
		int yLimit = this.getSize().height;
		
		System.out.print("width = "+xLimit+" height = "+yLimit);
		Random rand = new Random();
		
		for (Node node : nodes){
			node.setPosition(new Point(rand.nextInt(xLimit), rand.nextInt(yLimit)));
		}
	}
	
	private void drawNode(Node node, Graphics g){
		g.setColor(node.getColor());
		g.fillOval(node.getPosition().x, node.getPosition().y, 
				node.getSize(), node.getSize());
		g.drawString(node.getTag(), node.getPosition().x, node.getPosition().y - 10);
	}

	private void drawEdge(Node from, Node to, int weight, Graphics g){
		g.drawLine(from.getMidPoint().x, from.getMidPoint().y,
				to.getMidPoint().x, to.getMidPoint().y);
	}
	
	private void drawEdges(Graphics g){
		g.setColor(Color.RED);
		List<ArrayList<Integer>> adj_matrix = graph.getAdjacencyMatrix();
		List<Node> nodes = graph.getNodes();
		for (int i = 0; i < adj_matrix.size(); i++){
			for (int j = 0; j < adj_matrix.get(i).size(); j++){
				if(adj_matrix.get(i).get(j) > 0){
					drawEdge(nodes.get(i), nodes.get(j), 
							adj_matrix.get(i).get(j), g);
				}
			}
		}
	}
	
	private Node getNodeFromPosition(int x, int y){
		for (Node node : graph.getNodes()){
			if (x >= node.getPosition().x && x <= node.getPosition().x + node.getSize()
			&& y >= node.getPosition().y && y <= node.getPosition().y + node.getSize()){
				return node;
			}
		}
		return null;
	}

	
	public void visualize(Search search){

		SearchStatus status = search.getSearchStatus();		
		for (Node node : graph.getNodes()){
			node.setColor(Node.DEFAULT_COLOR);
		}
		Iterator<Node> iterator = status.getTraversalSequence().iterator();
		
		final Timer timer = new Timer(1000, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (iterator.hasNext()){
					Node node = iterator.next();
					if (!node.getTag().equals(""))
						node.setColor(Color.YELLOW);
					else {
						for (Node n : graph.getNodes()){
							n.setColor(Node.DEFAULT_COLOR);
						}
					}
						
					repaint();
				}else{
					if (status.isGoalFound())
						performFinalAnimation(status);			
					((Timer)e.getSource()).stop();
				}				
			}
		});
		
		timer.start();
		
	}
	
	private void performFinalAnimation(SearchStatus status){
		final Timer timer = new Timer(400, new ActionListener(){

			int count = 0;
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Iterator<Node> iterator = status.getFinalSequence().iterator();
				System.out.println("attempting final animation counte = " + count);
				while (iterator.hasNext()){
					if (count < 7){
						Node node = iterator.next();
						if (count % 2 == 0){
							node.setColor(Color.GREEN);
						}else{
							node.setColor(Node.DEFAULT_COLOR);
						}
						repaint();
					}else{
						((Timer)e.getSource()).stop();
						break;
					}
					
				}
				count++;
			}
			
		});
		timer.start();
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		if (selectedNode != null){
			selectedNode.setPosition(new Point(e.getX(), e.getY()));
		}
		repaint();
	}


	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		selectedNode = getNodeFromPosition(e.getX(), e.getY());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
