package ai.test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import ai.Graph;
import ai.Node;
import ai.Search.UninformedSearch.BFS;

public class BFSTest {

    @Test
    public void bfsSequenceShouldBeCorrect1() {

		Graph g = new Graph(true);

		g.addEdge(new Node("0"), new Node("1"));
		g.addEdge(new Node("0"), new Node("2"));
		g.addEdge(new Node("1"), new Node("2"));
		g.addEdge(new Node("2"), new Node("0"));
		g.addEdge(new Node("2"), new Node("3"));
		g.addEdge(new Node("3"), new Node("3"));
		
		BFS bfs = new BFS(g, "2", "1");
		Iterator<Node> sequence = bfs.getSearchStatus()
				.getTraversalSequence().iterator();
        // assert statements
        assertEquals("2", sequence.next().getTag());
        assertEquals("0", sequence.next().getTag());
        assertEquals("3", sequence.next().getTag());
        assertEquals("1", sequence.next().getTag());

    }


    @Test
    public void bfsSequenceShouldBeCorrect2() {

		Graph g = new Graph();
		g.addEdge(new Node("A"), new Node("B"));
		g.addEdge(new Node("A"), new Node("C"));
		g.addEdge(new Node("B"), new Node("D"));
		g.addEdge(new Node("B"), new Node("E"));
		g.addEdge(new Node("C"), new Node("F"));
		g.addEdge(new Node("C"), new Node("G"));
		
		BFS bfs = new BFS(g, "A", "D");
		Iterator<Node> sequence = bfs.getSearchStatus()
				.getTraversalSequence().iterator();
        // assert statements
        assertEquals("A", sequence.next().getTag());
        assertEquals("B", sequence.next().getTag());
        assertEquals("C", sequence.next().getTag());
        assertEquals("D", sequence.next().getTag());

    }
}
