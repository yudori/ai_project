package ai.test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import ai.Graph;
import ai.Node;
import ai.Search.UninformedSearch.BFS;
import ai.Search.UninformedSearch.DFS;

public class DFSTest {

    @Test
    public void dfsSequenceShouldBeCorrect1() {

		Graph g = new Graph(true);

		g.addEdge(new Node("0"), new Node("1"));
		g.addEdge(new Node("0"), new Node("2"));
		g.addEdge(new Node("1"), new Node("3"));
		g.addEdge(new Node("1"), new Node("4"));
		g.addEdge(new Node("2"), new Node("5"));
		g.addEdge(new Node("2"), new Node("6"));
		
		DFS dfs = new DFS(g, "0", "4");
		Iterator<Node> sequence = dfs.getSearchStatus()
				.getTraversalSequence().iterator();
        // assert statements
        assertEquals("0", sequence.next().getTag());
        assertEquals("2", sequence.next().getTag());
        assertEquals("6", sequence.next().getTag());
        assertEquals("5", sequence.next().getTag());
        assertEquals("5", sequence.next().getTag());
        

    }


    @Test
    public void dfsSequenceShouldBeCorrect2() {

		Graph g = new Graph(true);
		g.addEdge(new Node("A"), new Node("B"));
		g.addEdge(new Node("A"), new Node("C"));
		g.addEdge(new Node("B"), new Node("D"));
		g.addEdge(new Node("B"), new Node("E"));
		g.addEdge(new Node("C"), new Node("F"));
		g.addEdge(new Node("C"), new Node("G"));
		
		DFS dfs = new DFS(g, "A", "D");
		Iterator<Node> sequence = dfs.getSearchStatus()
				.getTraversalSequence().iterator();
        // assert statements
        assertEquals("A", sequence.next().getTag());
        assertEquals("B", sequence.next().getTag());
        assertEquals("D", sequence.next().getTag());
        assertEquals("E", sequence.next().getTag());

    }
}
