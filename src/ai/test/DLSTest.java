package ai.test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import ai.Graph;
import ai.Node;
import ai.Search.UninformedSearch.BFS;
import ai.Search.UninformedSearch.DFS;
import ai.Search.UninformedSearch.DLS;
import ai.Search.UninformedSearch.IDS;

public class DLSTest {

    @Test
    public void dlsSequenceShouldBeCorrect1() {

		Graph g = new Graph(true);

		g.addEdge(new Node("0"), new Node("1", new Node("0")));
		g.addEdge(new Node("0"), new Node("2", new Node("0")));
		g.addEdge(new Node("1"), new Node("2", new Node("1")));
		g.addEdge(new Node("1"), new Node("3", new Node("1")));
		g.addEdge(new Node("2"), new Node("3", new Node("2")));
		g.addEdge(new Node("2"), new Node("3", new Node("2")));
		
		DLS dls = new DLS(g, "1", "3", 1);
		Iterator<Node> sequence = dls.getSearchStatus()
				.getTraversalSequence().iterator();
        // assert statements
        assertEquals("2", sequence.next().getTag());
        assertEquals("0", sequence.next().getTag());
        assertEquals("3", sequence.next().getTag());
        assertEquals("1", sequence.next().getTag());

    }


    @Test
    public void dlsSequenceShouldBeCorrect2() {

		Graph g = new Graph(true);
		g.addEdge(new Node("A"), new Node("B", new Node("A")));
		g.addEdge(new Node("A"), new Node("C", new Node("A")));
		g.addEdge(new Node("B"), new Node("D", new Node("B")));
		g.addEdge(new Node("B"), new Node("E", new Node("B")));
		g.addEdge(new Node("C"), new Node("F", new Node("C")));
		g.addEdge(new Node("C"), new Node("G", new Node("C")));
		
		DLS dls = new DLS(g, "A", "D", 1);
		Iterator<Node> sequence = dls.getSearchStatus()
				.getTraversalSequence().iterator();
        // assert statements
        assertEquals("A", sequence.next().getTag());
        assertEquals("B", sequence.next().getTag());
        assertEquals("C", sequence.next().getTag());
        assertEquals("D", sequence.next().getTag());

    }
}
