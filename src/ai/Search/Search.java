package ai.Search;

import java.util.ArrayList;
import java.util.LinkedList;
import ai.Graph;
import ai.Node;

public abstract class Search {
	
	protected Graph graph;
	protected String startNodeTag;
	protected String endNodeTag;
	protected LinkedList<Node> visited;
	protected SearchStatus status;
	
	/**
	 * Search base class for searching through a graph
	 * @param graph the graph to be searched through
	 */
	public Search(Graph graph){
		this(graph, "", "");
	}
	
	/**
	 * Search base class for searching through a graph from a 
	 * given start node to the destination node
	 * @param graph the graph to be searched through
	 * @param startNodeTag the starting point node
	 * @param endNodeTag the node to be searched for
	 */
	public Search(Graph graph, String startNodeTag, String endNodeTag){
		this.graph = graph;
		this.startNodeTag = startNodeTag;
		this.endNodeTag = endNodeTag;
		this.status = new SearchStatus();
		resetVisited();
	}

	public Graph getGraph(){
		return this.graph;
	}
	
	public void setStartNode(String nodeTag){
		this.startNodeTag = nodeTag;
	}
	
	public Node getStartNode(){
		return getNodeFromTag(startNodeTag);
	}
	
	public void setDestinationNode(String nodeTag){
		this.endNodeTag = nodeTag;
	}
	
	public Node getDestinationNode(){
		return getNodeFromTag(endNodeTag);
	}
	
	public Node getNodeFromTag(String tag){
		ArrayList<Node> nodes = (ArrayList<Node>)graph.getNodes();
		int index = nodes.indexOf(new Node(tag));
		return index == -1 ? null : nodes.get(index);
	}
	
	protected boolean isVisited(String nodeTag){
		return visited.contains(getNodeFromTag(nodeTag)) ? true : false; 
	}
	
	protected void markVisited(String nodeTag){
		visited.add(getNodeFromTag(nodeTag));
	}
	
	protected void resetVisited(){
		this.visited = new LinkedList<>();
	}
	
	public LinkedList<Node> getVisited(){
		return this.visited;
	}

	/**
	 * This is an abstract method meant to be implemented by
	 * the subclass. It contains information about the search
	 * that has been performed
	 * @return SearchStatus object providing details 
	 * about the performed search
	 */
	public abstract SearchStatus getSearchStatus();

	
}
