package ai.Search;

import java.util.LinkedList;

import ai.Node;

public class SearchStatus{
	public boolean goalFound;
	public LinkedList<Node> traversalSequence;
	public LinkedList<Node> finalSequence;
	
	public SearchStatus(){
		this.goalFound = false;
		this.traversalSequence = new LinkedList<>();
		this.finalSequence = new LinkedList<>();
	}

	public boolean isGoalFound() {
		return goalFound;
	}

	public void setGoalFound(boolean goalFound) {
		this.goalFound = goalFound;
	}

	public LinkedList<Node> getTraversalSequence() {
		return traversalSequence;
	}

	public void setTraversalSequence(LinkedList<Node> traversalSequence) {
		this.traversalSequence = traversalSequence;
	}

	public LinkedList<Node> getFinalSequence() {
		return finalSequence;
	}

	public void setFinalSequence(LinkedList<Node> finalSequence) {
		this.finalSequence = finalSequence;
	}
	
	
}