package ai.Search.UninformedSearch;

import java.util.LinkedList;
import java.util.List;

import ai.Graph;
import ai.Node;
import ai.Search.Search;
import ai.Search.SearchStatus;

public class BFS extends Search{

	SearchStatus status;

	public BFS(Graph graph, String startNodeTag, String endNodeTag) {
		super(graph, startNodeTag, endNodeTag);
		// TODO Auto-generated constructor stub
		status = new SearchStatus();
		resetVisited();
		LinkedList<Node> queue = new LinkedList<Node>();
		markVisited(startNodeTag);
		queue.add(getStartNode());
		
		while (queue.size() > 0){
			Node node = queue.poll();
			List<Node> neighbours = getGraph().getNeighboursOf(node);
			if (status.isGoalFound()){
				System.out.print("Goal Found!");
				status.setFinalSequence(getVisited());
				break;
			}
			for (Node n : neighbours){
				if (!isVisited(n.getTag())){
					markVisited(n.getTag());
					queue.add(n);
				}
				if (n.getTag().equalsIgnoreCase(endNodeTag)){
					status.setGoalFound(true);
					break;
				}
			}
		}
		status.setTraversalSequence(getVisited());
	}

	@Override
	public SearchStatus getSearchStatus() {
		// TODO Auto-generated method stub
		return status;
	}

}
