package ai.Search.UninformedSearch;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import ai.Graph;
import ai.Node;
import ai.Search.Search;
import ai.Search.SearchStatus;


public class IDS extends Search{
	SearchStatus status;
	boolean stop;
	LinkedList<Node> nodeSequence = new LinkedList<Node>();
	/**
	 * Iterative Deepening Search Class for searching through a graph from a 
	 * given start node to the destination node to find the best depth limit. 
	 * It does this by gradually increasing limit first 0, then 1, then 2, and so on until the goal is found.
	 * @param graph the graph to be searched through
	 * @param startNode the starting point node
	 * @param goal the node to be searched for
	 */
		public IDS(Graph graph, String startNode, String goal){
			super(graph, startNode, goal);
			this.graph = graph;
			status = new SearchStatus();
			int depth = 0;
		    while(!status.isGoalFound())
		    {
		       System.out.println("Search Goal at Depth "+depth);
		        DLS(startNode, goal, depth);
		        System.out.println("-----------------------------");
		        depth++;
		        System.out.println("New depth = " + depth);
		    } 
			status.setTraversalSequence(nodeSequence);
			if (status.isGoalFound())
				status.setFinalSequence(nodeSequence);
		}
		
		public void DLS(String startNode, String goal, int depth) {
			// TODO Auto-generated constructor stub
			Stack<String> DLSstack = new  Stack<String>(); 
			DLSstack.push(startNode);
			resetVisited();
			
			
			while(!DLSstack.isEmpty()){
				String parentNode = DLSstack.pop();
				markVisited(parentNode);
				System.out.println(parentNode +" has been visited");
				nodeSequence.add(getNodeFromTag(parentNode));
				if(getNodeFromTag(parentNode) == getNodeFromTag(goal)){
					System.out.println("Goal found at start node");
					System.out.println("depth"+getNodeFromTag(parentNode).getDepth());
					status.setGoalFound(true);
					break;
				}
				else{
					List<Node> neighbors = getGraph().getNeighboursReverseOf(getNodeFromTag(parentNode));
					for(int i = 0; i < neighbors.size(); i++){
						Node nextNode = neighbors.get(i);
						System.out.println("depth for"+nextNode.getTag()+" "+nextNode.getDepth());
						if(nextNode != null &&  !isVisited(nextNode.getTag()) && nextNode.getDepth() <= depth){
							DLSstack.push(nextNode.getTag());
						}
						else{
							System.out.println("Goal not found");
							break;
						}
					}
				}
				
			}
			nodeSequence.add(new Node(""));
			
		}

		@Override
		public SearchStatus getSearchStatus() {
			// TODO Auto-generated method stub
			return status;
		}
	
}

 

   