package ai.Search.UninformedSearch;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import ai.Graph;
import ai.Node;
import ai.Search.Search;
import ai.Search.SearchStatus;


public class DLS extends Search{
	SearchStatus status;
	LinkedList<Node> nodeSequence = new LinkedList<Node>();
	/**
	 * Depth Limited Search Class for searching through a graph from a 
	 * given start node to the destination node by providing a depth limit to which it can search
	 * @param graph the graph to be searched through
	 * @param startNode the starting point node
	 * @param goal the node to be searched for
	 */
	public DLS(Graph graph, String startNode, String goal, int depth) {
		super(graph, startNode, goal);
		// TODO Auto-generated constructor stub
		status = new SearchStatus();
		Stack<String> DLSstack = new  Stack<String>(); 
		DLSstack.push(startNode);
		
		
		while(!DLSstack.isEmpty()){
			String parentNode = DLSstack.pop();
			markVisited(parentNode);
			System.out.println(parentNode +" has been visited");
			nodeSequence.add(getNodeFromTag(parentNode));
			if(getNodeFromTag(parentNode) == getNodeFromTag(goal)){
				System.out.println("Goal found at start node");
				System.out.println("depth"+getNodeFromTag(parentNode).getDepth());
				status.setGoalFound(true);
				break;
			}
			else{
				List<Node> neighbors = getGraph().getNeighboursReverseOf(getNodeFromTag(parentNode));
				for(int i = 0; i < neighbors.size(); i++){
					Node nextNode = neighbors.get(i);
					System.out.println("depth for"+nextNode.getTag()+" "+nextNode.getDepth());
					if(nextNode != null &&  !isVisited(nextNode.getTag()) && nextNode.getDepth() <= depth){
						DLSstack.push(nextNode.getTag());
					}
					else{
						System.out.println("Goal not found");
						break;
					}
				}
			}
			
		}
		status.setTraversalSequence(nodeSequence);
		if (status.isGoalFound())
			status.setFinalSequence(nodeSequence);
	}
	
	@Override
	public SearchStatus getSearchStatus() {
		// TODO Auto-generated method stub
		return status;
	}

}
