package ai.Search.UninformedSearch;

import java.util.LinkedList;
import java.util.List;

import ai.Graph;
import ai.Node;
import ai.Search.Search;
import ai.Search.SearchStatus;

public class BIDI extends Search{

	SearchStatus status;
	
	public BIDI(Graph graph, String startNodeTag, String endNodeTag) {
		super(graph, startNodeTag, endNodeTag);
		// TODO Auto-generated constructor stub
		status = new SearchStatus();
		LinkedList<String> q1 = new LinkedList<String>();
		LinkedList<String> q2 = new LinkedList<String>();
		q1.add(startNodeTag);
		markVisited(startNodeTag);
		q2.add(endNodeTag);
		markVisited(endNodeTag);
		
		String x;
		
		while (q1.size() > 0 && q2.size() > 0){
			if (q1.size() > 0){
				x = q1.pop();
				if (x.equals(endNodeTag) || q2.contains(x)){
					//success
					break;
				}
				List<Node> neighbours = getGraph().getNeighboursOf(getNodeFromTag(x));
				
				for (Node n : neighbours){
					if (!isVisited(n.getTag())){
						markVisited(n.getTag());
						System.out.println("Visited "+n);
						q1.add(n.getTag());
					}
				}
			}
			
			if (q2.size() > 0){
				x = q2.pop();
				if (x.equals(startNodeTag) || q1.contains(x)){
					//success
					break;
				}
				List<Node> neighbours = getGraph().getNeighboursOf(getNodeFromTag(x));
				
				for (Node n : neighbours){
					if (!isVisited(n.getTag())){
						markVisited(n.getTag());
						System.out.println("Visited "+n);

						q2.add(n.getTag());
					}
				}
			}
		}
		status.setTraversalSequence(getVisited());
	}

	@Override
	public SearchStatus getSearchStatus() {
		// TODO Auto-generated method stub		
		return status;
	}


}
