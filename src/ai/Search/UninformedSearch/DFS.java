package ai.Search.UninformedSearch;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import ai.Graph;
import ai.Node;
import ai.Search.Search;
import ai.Search.SearchStatus;


public class DFS extends Search{
	LinkedList<Node> nodeSequence = new LinkedList<Node>();
	SearchStatus status;
	/**
	 * Depth First Class for searching through a graph from a 
	 * given start node to the destination node
	 * @param graph the graph to be searched through
	 * @param startNode the starting point node
	 * @param endNode the node to be searched for
	 */
	public DFS(Graph graph, String startNode, String endNode) {
		super(graph, startNode, endNode);
		// TODO Auto-generated constructor stub
		status = new SearchStatus();
		Stack<String> DFSstack = new  Stack<String>(); 
		DFSstack.push(startNode);
		
		while(!DFSstack.isEmpty()){
			String parentNode = DFSstack.pop();
			markVisited(parentNode);
			System.out.println(parentNode +" has been visited");
			nodeSequence.add(getNodeFromTag(parentNode));
			if(getNodeFromTag(parentNode) == getNodeFromTag(endNode)){
				System.out.println("Goal found at start node");
				status.setGoalFound(true);
				break;
			}
			else{
				List<Node> neighbors = getGraph().getNeighboursReverseOf(getNodeFromTag(parentNode));
				for(int i = 0; i < neighbors.size(); i++){
					Node nextNode = neighbors.get(i);
					if(nextNode != null &&  !isVisited(nextNode.getTag())){
						DFSstack.push(nextNode.getTag());
					}
					else{
						System.out.println("Goal not found");
						break;
					}
				}
			}
			
		}
		status.setTraversalSequence(nodeSequence);
		if (status.isGoalFound())
			status.setFinalSequence(nodeSequence);
	}
	@Override
	public SearchStatus getSearchStatus() {
		// TODO Auto-generated method stub
		return status;
	}


}
