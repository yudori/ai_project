package ai;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * This class is an ADT representing a graph structure 
 * for performing various graph functions
 * @author ai_team group 3
 *
 */
public class Graph{

	private boolean isWeighted;
	private boolean isDirected;
	private List<Node> nodes;
	private List<ArrayList<Integer>> adj_matrix;

	/**
	 * Default non-weighted, non-directed Graph class constructor
	 */
	public Graph(){
		this(false, false);
	}	

	/**
	 * Graph class constructor
	 * @param isWeighted boolean value, true if graph is a weighted
	 * graph, false otherwise
	 */
	public Graph(boolean isDirected){
		this(isDirected, false);	
	}	

	/**
	 * Graph class constructor
	 * @param isDirected boolean value, true if graph is a directed
	 * graph, false otherwise
	 * @param isWeighted boolean value, true if graph is a weighted
	 * graph, false otherwise
	 */
	public Graph(boolean isDirected, boolean isWeighted){
		this.isDirected = isDirected;
		this.isWeighted = isWeighted;
		this.nodes = new ArrayList<Node>();
		this.adj_matrix = new ArrayList<ArrayList<Integer>>();	
	}
	
	/**
	 * helper method to generate a random graph
	 * @return generated graph
	 */
	public static Graph generateRandomGraph(){
		Random rand = new Random();
		String[] possibleNodes = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
		int numberOfEdges = 1 + rand.nextInt(possibleNodes.length * 2);
		Graph graph = new Graph();
		for (int i = 0; i < numberOfEdges; i++){
			Node n1 = new Node(possibleNodes[rand.nextInt(possibleNodes.length)]);
			Node n2 = new Node(possibleNodes[rand.nextInt(possibleNodes.length)]);
			graph.addEdge(n1, n2, 1);
		}
		return graph;
	}
	
	/**
	 * helper method for getting the index of a node stored in the graph
	 * @param node - the node
	 * @return index of tag in nodes list
	 */
	private int getIndexOfNode(Node node){
		return nodes.indexOf(node);
	}

	/**
	 * initializes adjacency matrix with zeros
	 * @param nodeSize height and width of matrix
	 */
	private void initializeMatrix(int nodeSize){

		ArrayList<Integer> temp;
		for (int i = 0; i < nodeSize; i++){
			Integer[] arr = new Integer[nodeSize];
			temp = new ArrayList<Integer>(Arrays.asList(arr));
			Collections.fill(temp, 0);
			adj_matrix.add(temp);
			
		}
	}
	
	/**
	 * Adds an edge to the graph
	 * @param from - the source node
	 * @param to - the destination node
	 */
	public void addEdge(Node from, Node to){
		addEdge(from, to, 1);
	}
	
	/**
	 * Adds an edge to the graph
	 * @param from - the source node
	 * @param to - the destination node
	 * @param weight - the weight to be attributed to the edge
	 */
	public void addEdge(Node from, Node to, int weight){
		if (!nodes.contains(from))
			addNode(from);
		if (!nodes.contains(to))
			addNode(to);
		int index1 = getIndexOfNode(from);
		int index2 = getIndexOfNode(to);
		adj_matrix.get(index1).set(index2, weight);
		if (!isDirected){
			adj_matrix.get(index2).set(index1, weight);
		}
	}

	/**
	 * Adds a node to the graph
	 * @param node - the node to be added
	 */
	public void addNode(Node node){
		if (!nodes.contains(node)){			
			for (int i = 0; i < getNumberOfNodes(); i++){
				adj_matrix.get(i).add(0);
			}
			Integer[] arr = new Integer[getNumberOfNodes() + 1];
			ArrayList<Integer> temp = new ArrayList<Integer>(Arrays.asList(arr));
			Collections.fill(temp, 0);
			adj_matrix.add(temp);
			nodes.add(node);			
		}		
	}
	
	public int getWeight(Node from, Node to) throws WeightException{
		if (isWeighted){
			return adj_matrix.get(nodes.indexOf(from))
					.get(nodes.indexOf(to));
		}
		throw new WeightException("Not a Weighted graph");
	}
	
	/**
	 * Gets number of nodes in graph
	 * @return number of nodes
	 */
	public int getNumberOfNodes(){
		return nodes.size();
	}

	/**
	 * gets the adjacency matrix of the graph
	 * @return a list of lists representing the adjacency matrix
	 */
	public List<ArrayList<Integer>> getAdjacencyMatrix(){
		return adj_matrix;
	}
	
	/**
	 * gets the nodes of the graph
	 * @return a list of Nodes in the graph
	 */
	public List<Node> getNodes(){
		return nodes;
	}
	
	/**
	 * Checks if a movement can be made from a node
	 * to another. This is equivalent to a normal connection 
	 * in an undirected graph
	 * @param from the source node
	 * @param to the destination node
	 * @return boolean value is connected
	 */
	public boolean isConnected(Node from, Node to){
		if (!nodes.contains(from) || !nodes.contains(to))
			return false;
		
		if (adj_matrix.get(nodes.indexOf(from)).get(nodes.indexOf(to)) > 0)
			return true;
		
		return false;
	}
	
	/**
	 * Gets a list of neighbour nodes for undirected graph
	 * and a list of "nodes to" from a given node
	 * @param node - the source node
	 * @return list of nodes
	 */
	public List<Node> getNeighboursOf(Node node){
		List<Node> neighbourNodes = new ArrayList<Node>();
		for (Node n : nodes){
			if (isConnected(node, n))
				neighbourNodes.add(n);
		}
		Collections.sort(neighbourNodes, new NodeComparator());
		return neighbourNodes;
	}
	
	/**
	 * Gets a list of neighbour nodes for undirected graph
	 * and a list of "nodes to" from a given node
	 * @param node - the source node
	 * @return list of nodes
	 */
	public List<Node> getNeighboursReverseOf(Node node){
		List<Node> neighbourNodes = new ArrayList<Node>();
		for (Node n : nodes){
			if (isConnected(node, n))
				neighbourNodes.add(n);
		}
		Collections.sort(neighbourNodes, new NodeComparator());
		Collections.reverse(neighbourNodes);
		return neighbourNodes;
	}

	public class NodeComparator implements Comparator<Node>{

		@Override
		public int compare(Node n1, Node n2) {
			// TODO Auto-generated method stub
			return n1.getTag().compareTo(n2.getTag());
		}		
	}
	
	public class WeightException extends Exception{

		public WeightException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
		
	}
}
